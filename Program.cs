﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Viselitca
{
    class Program
    {
        static void Main(string[] args)
        {
            int ch = Game.Play();
            Console.Clear();
            if (ch == 0) Console.WriteLine("Вы проиграли");
            else Console.WriteLine("Вы выиграли");
            Console.ReadKey();
        }
    }

    class Game
    {
        string word;
        int lives;

        public char[] guessletter { get; set; }

        public Game (string word)
        {
            this.word = word;
            this.guessletter = new char[word.Length];
            this.lives = 5;

        }

        public Game Сheck(Game str, char ch)
        {
            int k = 0;
            for (int i = 0; i < str.word.Length; i++)
            {
                if (ch == str.word[i])
                {
                    str.guessletter[i] = ch;
                    k++;
                }
            }
            if (k == 0) str.lives--;
            return str;
        }

        public static string Randomaiser()
        {
            Random rnd = new Random();
            string[] file_random = File.ReadAllLines("word.txt");
            int index = rnd.Next(file_random.Length);

            return file_random[index];
        }

        public static int Play ()
        {
            string word = Randomaiser();
            Game str = new Game(word);
            int k = 0;

           do
            {
                k = 0;
                Console.SetCursorPosition(0, 1);
                for (int i = 0; i < word.Length; i++)
                {
                    if (str.guessletter[i] == 0) Console.Write("_ ");
                    else
                    {
                        Console.Write($"{str.guessletter[i]} ");
                        k++;
                    }
                }
                Console.Write($"{str.lives }");

                Console.SetCursorPosition(0, 0);
                char ch = Console.ReadKey().KeyChar;
                str = str.Сheck(str, ch);
            }
            while (str.lives > 0 && k != word.Length) ;
            return str.lives;
        }

    }

}